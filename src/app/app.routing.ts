import { Routes, RouterModule } from "@angular/router";

import { ModuleWithProviders } from "@angular/core";
import { IndexPageComponent } from "./components/index-page/index-page.component";
import { CheckoutComponent } from "./components/checkout/checkout.component";


const appRoute : Routes = [
    {path: '', component: IndexPageComponent},
    {path: 'checkout', component: CheckoutComponent},
    
]

export const appRoutingPorviders : any[]=[];
export const routing: ModuleWithProviders<any> = RouterModule.forRoot(appRoute, { onSameUrlNavigation: 'reload' });
