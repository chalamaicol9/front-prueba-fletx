import { TestBed } from '@angular/core/testing';

import { GLOBALService } from './global.service';

describe('GLOBALService', () => {
  let service: GLOBALService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GLOBALService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
