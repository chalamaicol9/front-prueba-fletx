import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from "rxjs";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Product } from '../components/interfaces/Product';


@Injectable({
  providedIn: 'root'
})
export class GLOBALService {
  public static API_ENDPOINT = 'http://localhost:3000/';
  private cartTotal = new BehaviorSubject(0);
  cart: Product[] = [];


  constructor( private _http: HttpClient,) { 
  
  }

  getProductos(): Observable<any>{
    return this._http.get(GLOBALService.API_ENDPOINT + 'producto');
  }

  getCartTotal() {
    return this.cartTotal.asObservable();
  }
  
  addToCart(product: Product) {
    this.cart.push(product);
    this.cartTotal.next(this.cartTotal.value + product.Precio);
  }
  getCart() {
    return this.cart;
  }
  removeProductFromCart(product: Product) {
    const index = this.cart.indexOf(product);
    if (index >= 0) {
      this.cart.splice(index, 1);
      this.cartTotal.next(this.cartTotal.value - product.Precio);
    }
  }
  comprar(productos: Product[]){
    return this._http.post(GLOBALService.API_ENDPOINT + 'producto/comprar', productos);
}
clearCart() {
  this.cart = [];
  this.cartTotal.next(0);
}

}
