import { Component, OnInit } from '@angular/core';
import { GLOBALService } from 'src/app/service/global.service';
import { Router } from '@angular/router';
declare var $:any;
declare var iziToast:any;

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit{
  cart: any = [];
  cartTotal: any;
  constructor(private _globalService: GLOBALService,private router:Router) { }

  ngOnInit() {
    this.cart = this._globalService.getCart();
    this._globalService.getCartTotal().subscribe(total => {
      this.cartTotal = total.toLocaleString('de-DE');
      
    });
    console.log(this.cart);
  }
  
  public comprar() {
    this._globalService.comprar(this.cart).subscribe(
        (data: any) => {
            iziToast.success({
                title: 'Exito',
                position: 'topRight',
                message: 'Compra realizada con exito.'
            });
            console.log(data);
            this.cartTotal = 0;
            this._globalService.clearCart(); // Limpia el carrito en el servicio global
            this.router.navigate(['']);
        },
        error => {
            iziToast.error({
                title: 'Error',
                position: 'topRight',
                message: 'No hay suficiente stock para completar la compra.'
            });
            console.log(error);
        }
    );
}


}
