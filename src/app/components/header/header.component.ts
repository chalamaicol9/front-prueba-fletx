import { Component,OnInit} from '@angular/core';
import { Product } from '../interfaces/Product';
import { GLOBALService } from 'src/app/service/global.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';

declare var $: any;


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit{
  menuVisible = true;
  cart: Product[] = [];
  cartTotal: any;

  constructor(private _globalService: GLOBALService,private modalService: NgbModal,
    private router: Router
  ) { }

  ngOnInit() {
    this.cart = this._globalService.getCart();
    this._globalService.getCartTotal().subscribe(total => {
      this.cartTotal = total.toLocaleString('de-DE');
      
    });
    console.log(this.cart);
  }

  toggleMenu() {
    this.menuVisible = !this.menuVisible;
  }
  removeProduct(product: Product) {
    const index = this.cart.indexOf(product);
    if (index >= 0) {
        this.cart.splice(index, 1);
        this._globalService.removeProductFromCart(product);
        this.updateTotal();
    }
}

updateTotal() {
    this.cartTotal = this.cart.reduce((total, product) => total + product.Precio, 0);
}
  open(content: any) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
    }, (reason) => {
    });
  }
  finalizarCompra() {
    // Guardar los productos del carrito en un servicio o estado global
    this._globalService.getCart();

    // Redirigir al usuario a la página de checkout
    this.router.navigate(['/checkout']);
}
}



