export interface Product {
    ID: number;
    Fecha_creacion: string;
    Nombre: string;
    Precio: number;
    Valor: number;
    Stock: number;
    createdAt: string;
    updatedAt: string;
    CategoriaID: number;
  }
