import { Component, OnInit } from '@angular/core';
import { GLOBALService } from 'src/app/service/global.service';

@Component({
  selector: 'app-index-page',
  templateUrl: './index-page.component.html',
  styleUrls: ['./index-page.component.css']
})
export class IndexPageComponent implements OnInit{
  arrayProductos: any = [];
  cartTotal: any;
  constructor(private _globalService: GLOBALService) { }

  ngOnInit(){
  this.getProductos();
  this._globalService.getCartTotal().subscribe(total => {
    this.cartTotal = total;
  });
  }

  getProductos(){
    this._globalService.getProductos().subscribe(
      (data:any) => {
        if(data){
          this.arrayProductos = data;
         
        }else{
          this.arrayProductos = [];
        } 
      },
      error => {
        console.log(error);
      }
    );
  }
  addToCart(product: any) {
    console.log(product);
    this._globalService.addToCart(product);
  }


}
