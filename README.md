

## Instalación

1. Clona este repositorio en tu máquina local

2. Navega al directorio del proyecto

3. Instala las dependencias necesarias utilizando npm:
npm install 

## Uso

Una vez que hayas instalado las dependencias, puedes ejecutar el proyecto usando el siguiente comando: ng serve



